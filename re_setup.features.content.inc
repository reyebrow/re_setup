<?php
/**
 * @file
 * re_setup.features.content.inc
 */

/**
 * Implements hook_content_defaults().
 */
function re_setup_content_defaults() {
  $content = array();

  $content['homepage'] = (object) array(
    'exported_path' => 'home',
    'title' => 'Homepage',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'page',
    'language' => 'en',
    'created' => '1350425984',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'homepage',
    'body' => array(),
  );

return $content;
}
